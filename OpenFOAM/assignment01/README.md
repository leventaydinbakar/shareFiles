# This is assignment of Lecture 2

# How to run this script?

After you change content in
* main.tex
* parts/01_introduction.tex
* parts/02_methodology.tex
* parts/03_results.tex
* parts/04_contribution.tex
files, and generate and copy
* mesh.png
* velocity.png
* pressure.png
into the `figures/` folder,
you can generate the pdf running 

```bash
pdflatex main.tex
```

on a terminal window in the directory where the `main.tex` file is located.
`main.pdf` will be generated. If you see an error, 
please read it and try to understand what you make wrong.
If you do not see any error but the PDF is not as you assume,
run the `pdflatex` command again.
