#!/bin/sh
#   Author        : Levent Aydinbakar
#   Date          : 15-06-47---26-02-2024
#   Last Modified : 15-09-51---26-02-2024

#!/bin/sh

# Base directory containing lecture directories
base_dir="OpenFOAM"

# Base URL
base_url="https://soscfd.com"

# Loop from 1 to 14
for i in $(seq -w 1 14); do
    # Define directory and file path
    dir="${base_dir}/lecture${i}"
    file="${dir}/README.md"

    # Check if directory exists, create if not
    mkdir -p "$dir"

    # Write content to README.md file
    echo "# This is lecture${i} README file. Go to [soscfd.com](${base_url}/lecture${i}-0) for more information." > "$file"
done

echo "README files have been created/updated."

