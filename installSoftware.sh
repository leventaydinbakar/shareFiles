#!/bin/sh
#   Author        : Levent Aydinbakar
#   Date          : 23-29-58---04-09-2024
#   Last Modified : 02-43-47---05-09-2024

# Exit script on error
set -e

# Log script activities
LOGFILE="/tmp/install_log.txt"
exec > >(tee -i $LOGFILE)
exec 2>&1

# Update system and install necessary base packages
echo "Updating system and installing base packages..."
sudo apt update && sudo apt upgrade -y
sudo apt install -y zsh vim screen git curl python3 python3-pip software-properties-common

# Install Python packages
echo "Installing Python modules..."
pip3 install numpy scipy pandas

# Set up /opt directory and permissions
echo "Setting up /opt directory..."
sudo chown -R $USER:$USER /opt

# Clone configuration files from GitLab
echo "Cloning configuration files from GitLab..."
git clone https://gitlab.com/leventaydinbakar/ubuntuSetupFiles.git ~/ubuntuSetupFiles

# Copy the .zshrc from the GitLab repo to home directory
echo "Configuring zsh, vim and screen..."
rm -rf ~/.zsh ~/.zshenv
cp ~/ubuntuSetupFiles/.zsh ~/.zsh
cp ~/ubuntuSetupFiles/.zshenv ~/.zshenv

rm -rf ~/.vimrc ~/.viminfo ~/.vim
cp -r ~/ubuntuSetupFiles/.vimrc ~/.vimrc
cp -r ~/ubuntuSetupFiles/.viminfo ~/.viminfo
cp -r ~/ubuntuSetupFiles/.vim ~/.vim

rm -rf ~/.screenrc
cp -r ~/ubuntuSetupFiles/.screenrc ~/.screenrc

# Install ParaView 5.13
echo "Installing ParaView 5.13..."
PARAVIEW_URL="https://www.paraview.org/files/v5.13/ParaView-5.13.0-MPI-Linux-Python3.10-x86_64.tar.gz"
PARAVIEW_DIR="/opt/paraview513"
wget $PARAVIEW_URL -O /tmp/paraview.tar.gz
sudo mkdir -p $PARAVIEW_DIR
sudo tar -xzf /tmp/paraview.tar.gz -C $PARAVIEW_DIR --strip-components=1
rm /tmp/paraview.tar.gz

# Set ParaView alias in .zshrc
#echo 'alias paraview="/opt/paraview513/bin/paraview"' >> ~/.zsh/.zshrc

# Install OpenFOAM 2312
echo "Installing OpenFOAM 2312..."
curl https://dl.openfoam.com/add-debian-repo.sh | sudo bash
sudo apt-get update
sudo apt-get install -y openfoam2312-default

# Move OpenFOAM to /opt
echo "Moving OpenFOAM 2312 to /opt..."
sudo mv /usr/lib/openfoam/openfoam2312 /opt

# Set OpenFOAM alias in .zshrc
#echo 'alias of2312="source /opt/openfoam2312/etc/bashrc"' >> ~/.zsh/.zshrc
#echo alias of2312="emulate sh -c 'source /opt/openfoam2312/etc/bashrc'"'

# Install FEniCS
echo "Installing FEniCS 2013.2.0.13.dev0..."
sudo add-apt-repository ppa:fenics-packages/fenics -y
sudo apt-get update
sudo apt-get install -y fenics

# Install LaTeX with pgfplots and tikz
echo "Installing LaTeX..."
sudo apt-get install -y texlive-full

# Install imagemagick
echo "Installing ImageMagick..."
sudo apt-get install imagemagick

# Modify ImageMagick's policy.xml to allow PDF conversion
echo "Modifying ImageMagick policy.xml to comment out PDF restrictions..."
sudo sed -i 's/.*PDF.*/<!-- & -->/' /etc/ImageMagick-6/policy.xml

# Install SSH...
sudo apt install -y openssh-server
sudo systemctl enable ssh

# Final message
echo "Installation complete! Please restart your terminal or run 'exec zsh' to apply zsh changes."

