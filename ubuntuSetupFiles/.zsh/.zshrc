#   Author        : Levent Aydinbakar
#   Date          : 09-13-01---12-05-2021
#   Last Modified : 01-40-10---05-09-2024

autoload -U compinit
compinit

autoload -U colors
colors

umask 002

zstyle ':completion:*' list-colors ''

# setopt multiosset

DIRSTACKSIZE=20

## please refer to "man zshoptions" for help

setopt auto_pushd           # auto directory pushd that you can get dirs by cd -[tab]
setopt pushd_to_home        # back to home with cd without any arguments
setopt list_packed          # compacked complete list display
setopt no_clobber           # prevent overwrite ridirection
setopt no_unset             # prevent using undefined variables
setopt no_hup               # do not kill backgound jobs when logging out
setopt numeric_glob_sort    # regard number as numerical value and sort them in ascending order
setopt nolistbeep           # no beep sound when complete list displayed
setopt no_beep              # no beep sound when entering a wrong command
setopt ignore_eof           # do not log out by C-d
setopt pushd_ignore_dups    # remove older one of duplication in directory stack
setopt interactive_comments # allow to use comment while entering command
setopt complete_in_word     # try to complete a word by completion
setopt correct              # command correct edition before each completion attempt
setopt always_last_prompt   # show file list with cursor stayed in the current position
setopt auto_param_slash     # automatically add "/" when completing directory name
setopt mark_dirs            # add "/" when a directory is selected in file name list
setopt auto_menu            # automatically complete in order by typing completion key
setopt extended_glob        # completion by extended glob
setopt list_types           # show distinguishing mark in a completion list
setopt auto_cd

bindkey "Tab" menu-complete

## insert all expansions for expand completer
zstyle ":completion:*:expand:*" tag-order all-expansions

## formatting and messages
zstyle ":completion:*" verbose yes
zstyle ":completion:*" group-name ""

zstyle ":completion:*:default" menu select=1
zstyle ":completion:*" completer _complete _approximate _prefix _match # _expand
zstyle ":completion:*:options" description "yes"

## match uppercase from lowercase
zstyle ":completion:*" matcher-list "m:{a-z}={A-Z}"

## offer indexes before parameters in subscripts
zstyle ":completion:*:*:-subscripts-:*" tag-order indexes parameters

## Environment variable configuration

export LANG=en_US.UTF-8

## Default shell configuration

## Set prompts
setopt prompt_subst
PROMPT='%{%(?.%{$fg_bold[green]%}.%{$fg_bold[red]%})%}%m%{$reset_color%}%# '
GROUP_PROMPT=`id | cut -d= -f4 | sed -r "s/([[:digit:]]+)\(([[:alnum:]]+)\)/%\1(g:\2:)/g" | sed "s/,//g"`
RPROMPT='%{$fg[white]%}[%{$fg[yellow]%}$GROUP_PROMPT%{%{$reset_color%}$fg[white]%}]%{$reset_color%} %{$fg_bold[blue]%}%~%{$reset_color%} %t'
SPROMPT='%{33m%} %BCurrent> '\''%r'\'' [Yes, No, Abort, Edit]%{[m%}%b '

## time
REPORTTIME=10
TIMEFMT="\
  The name of this job.               :%J
  CPU seconds spent in user mode.     :%U
  CPU seconds spent in kernel mode.   :%S
  Elapsed time in seconds.            :%E
  The CPU percentage.                 :%P
  The maximum memory usage            :%M Kb"
  

## set terminal title including current directory

case "${TERM}" in
kterm*|xterm)
  precmd() {
    echo -ne "\033]0;${USER}@${HOST%%.*}:${PWD}\007"
  }
  ;;
esac

## Command history

HISTFILE="${HOME}/.zhistory"
HISTSIZE=10000
SAVEHIST=10000
setopt extended_history      # manage starting time and spent time of a command
setopt hist_ignore_dups      # ignore duplication command history list
setopt hist_ignore_all_dups  # delete older one in case of already registered command
setopt share_history         # share command history data
setopt hist_reduce_blanks    # delete extra blanks

zstyle ":auto-fu:highlight" completion fg=black,bold

## Keybind configuration configuration

bindkey -e

## historical backward/forward search with linehead string binded to ^P/^N
autoload history-search-end
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end  history-search-end
bindkey "^P" history-beginning-search-backward-end
bindkey "^N" history-beginning-search-forward-end

setopt complete_aliases # aliased ls needs if file/dir completions work

alias la="ls -a"
alias ll="ls -l"
alias l="ls"
alias sl="ls"
alias ls="ls -F --color"
alias lss="ls -la"
alias s="ls -la"
alias maintex='pdflatex main.tex ; pdflatex main.tex'
alias cleantex='rm -rf *.aux *.log'

# OpenFOAM version selection (example)
emulate sh -c 'source /opt/openfoam2312/etc/bashrc'
alias of2312="emulate sh -c 'source /opt/openfoam2312/etc/bashrc'"
alias of10="emulate sh -c 'source /opt/openfoam10/etc/bashrc'"
#alias of2312="source /opt/software/openfoam2312/etc/bashrc"
#alias of10="source /opt/software/openfoam10/etc/bashrc"

# ParaView alias
alias paraview='/opt/paraview513/bin/paraview'

# Create a safe rm command
# Deleted files will be moved to ~/.trash, which is cleared on session exit
TRASH_DIR="${HOME}/.trash"

# Create trash directory if it doesn't exist
mkdir -p "$TRASH_DIR"

# Define a safe rm function that moves files to the trash instead of deleting them
function safe_rm() {
    local options=()
    local files=()

    # Separate options (e.g., -r, -f) from files
    for arg in "$@"; do
        if [[ "$arg" == -* ]]; then
            options+=("$arg")
        else
            files+=("$arg")
        fi
    done

    # Move the files to the trash directory
    if [[ ${#files[@]} -gt 0 ]]; then
        for file in "${files[@]}"; do
            mv "$file" "$TRASH_DIR/"
#            echo "Moved to trash: $file"
        done
    fi
}

# Override rm to use the safe_rm function
alias rm="safe_rm"

# Function to clear the trash on session exit
function clear_trash() {
    find "$TRASH_DIR" -mindepth 1 -delete
}

# Run clear_trash when the session ends
trap clear_trash EXIT

path=($path /opt/soscfd/lib)
path=($path /opt/soscfd/LIB)

