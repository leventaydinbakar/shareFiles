syntax on
"set mouse=a
set background=dark
set tw=80
set ts=4
set sw=2
set autoindent
set shiftwidth=2
set number
set modeline
"set cinoptions=g0,t0,(0,:0

set switchbuf=split,useopen
set list
set listchars=tab:>-
set dictionary=
set textwidth=0
set nowrap
set invlist
set wrap

set fileformats=unix,dos,mac
set statusline=%<%f\ %m%r%h%w%{'['.(&fenc!=''?&fenc:&enc).']['.&ff.']'}%=%l,%c%V%8P:%t
set laststatus=2 
set hlsearch
" Ignore capital characters when searching
set ignorecase
" Overwrite Ignore capital characters when searching in capital
set smartcase
set showcmd
set showmatch
set expandtab
set noswapfile
set backupdir=~/.vim/backup
set backup
set incsearch
set paste

autocmd BufNewFile,BufRead *.md set filetype=markdown


nnoremap C-k k
nnoremap C-j j
nnoremap C-h h
nnoremap C-l l
nnoremap j gj
nnoremap k gk
nnoremap z? z=

function! GetStatusEx()
 let str = ''
 let str = str . '[' . &fileformat . ']'
 if has('multi_byte') && &fileencoding != ''
 let str = str . '[' . &fileencoding . ']'
 endif
 return str
endfunction

augroup InsertHook
autocmd!
autocmd InsertEnter * highlight StatusLine guifg=#ccdc90 guibg=#2E4340
autocmd InsertLeave * highlight StatusLine guifg=#2E4340 guibg=#ccdc90
augroup END

" Uncomment the following to have Vim jump to the last position when
" reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal! g'\"" | endif
endif

" Map the F5 key to run a Python script inside Vim.
" I map F5 to a chain of commands here.
" :w saves the file.
" <CR> (carriage return) is like pressing the enter key.
" !clear runs the external clear screen command.
" !python3 % executes the current file with Python.
nnoremap <f2> :w <CR>:!clear <CR>:!python3 % <CR>
nnoremap <f3> :w <CR>:!clear <CR>:!bash % <CR>
nnoremap <f4> :w <CR>:!clear <CR>:!gnuplot -p % <CR>
nnoremap <f5> :w <CR>:!clear <CR>:!pdflatex % <CR>

" Resize split windows using arrow keys by pressing:
" CTRL+UP, CTRL+DOWN, CTRL+LEFT, or CTRL+RIGHT.
noremap <c-up> <c-w>+
noremap <c-down> <c-w>-
noremap <c-left> <c-w>>
noremap <c-right> <c-w><

" If Vim version is equal to or greater than 7.3 enable undofile.
" This allows you to undo changes to a file even after saving it.
if version >= 703
  set undodir=~/.vim/backup
  set undofile
  set undoreload=10000
endif

" Set tikz tex file header

" autocmd bufnewfile *.tex so ~/.vim/author_info.txt
" autocmd bufnewfile *.tex exe "g/Date .*/s//Date          : " .strftime("%H-%M-%S---%d-%m-%Y")
" autocmd Bufwritepre,filewritepre *.tex execute "normal ma"
" autocmd Bufwritepre,filewritepre *.tex exe "g/Last Modified .*/s/Last Modified :.*/Last Modified : " .strftime("%H-%M-%S---%d-%m-%Y")
" autocmd bufwritepost,filewritepost *.tex execute "normal `a"

" Set sh file header

autocmd bufnewfile *sh so ~/.vim/author_info_sh.txt
autocmd bufnewfile *sh exe "g/Date .*/s//Date          : " .strftime("%H-%M-%S---%d-%m-%Y")
autocmd Bufwritepre,filewritepre *sh execute "normal ma"
"autocmd Bufwritepre,filewritepre *sh exe "g/Last Modified .*/s/Last Modified :.*/Last Modified : " .strftime("%c")
autocmd Bufwritepre,filewritepre *sh exe "g/Last Modified .*/s/Last Modified :.*/Last Modified : " .strftime("%H-%M-%S---%d-%m-%Y")
" autocmd bufwritepost,filewritepost *sh execute "normal `a"

" Set py file header

autocmd bufnewfile *.py so ~/.vim/author_info_py.txt
autocmd bufnewfile *.py exe "g/Date .*/s//Date          : " .strftime("%H-%M-%S---%d-%m-%Y")
autocmd Bufwritepre,filewritepre *.py execute "normal ma"
autocmd Bufwritepre,filewritepre *.py exe "g/Last Modified .*/s/Last Modified :.*/Last Modified : " .strftime("%H-%M-%S---%d-%m-%Y")
autocmd bufwritepost,filewritepost *.py execute "normal `a"

" Set cpp file header

autocmd bufnewfile *.cpp so ~/.vim/author_info_cpp.txt
autocmd bufnewfile *.cpp exe "g/Date .*/s//Date          : " .strftime("%H-%M-%S---%d-%m-%Y")
autocmd Bufwritepre,filewritepre *.cpp execute "normal ma"
autocmd Bufwritepre,filewritepre *.cpp exe "g/Last Modified .*/s/Last Modified :.*/Last Modified : " .strftime("%H-%M-%S---%d-%m-%Y")
autocmd bufwritepost,filewritepost *.cpp execute "normal `a"
nnoremap <f2> :w <CR>:!clear <CR>:!python3 % <CR>
nnoremap <f4> :w <CR>:!clear <CR>:!pdflatex % <CR>
